package com.inezpre5.modelo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface UsuarioCrud extends CrudRepository<Usuario, Long>{

	void deleteById(Optional<Usuario> findById);

	

}
